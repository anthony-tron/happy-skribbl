# happy-skribbl

A simple CLI and library to play skribbl.io with custom words.

A bot creates and joins a room, waits for the players to join and leaves after starting the game.

## Usage

Create words.json:

```json
[
  "apple",
  "eagle",
  "cheese",
  "france",
  "science",
  "mystery",
  "mustache",
  "computer",
  "skydiving",
  "cellphone"
]
```

`happy-skribbl words.json`

```
Join with https://skribbl.io/?zIaGf6os09Zz.
Game will automatically start in 10 seconds...
```

> Limitation: custom words will be lost at the end of the game.

Waiting for 30 seconds: `happy-skribbl  -d30 words.json`

Waiting for key press: `happy-skribbl -w words.json`
