#! /usr/bin/env node

const fs = require('fs');
const { program } = require('commander');
const { createGame } = require('../lib');
const { pause, sleep } = require('../utils/wait');

const defaultOrigin = 'https://skribbl.io:4999';

const defaultParams = {
  avatar: [9, 10, 23, -1],
  code: '',
  createPrivate: true,
  join: '',
  language: 'English',
  name: 'Animator',
};

program
  .argument('filename', 'JSON file that is an array of string')
  .option(
    '-d, --delay <seconds>',
    'Delay in seconds before the game starts',
    10,
  )
  .option('-w, --wait', 'Wait for key press before starting the game', false)
  .action(async (path) => {
    const content = fs.readFileSync(path, { encoding: 'utf-8' });
    const data = JSON.parse(content);

    if (!Array.isArray(data) || data.some((e) => typeof e !== 'string')) {
      console.error('Expected an array of strings.');
      process.exit(1);
    }

    const { delay, wait } = program.opts();

    try {
      const game = await createGame(defaultOrigin, defaultParams);

      console.log(`Join with https://skribbl.io/?${game.key}.`);

      if (wait) {
        console.log('Game ready to start, press Enter to start.');
        await pause(process.stdin);
      } else {
        console.log(`Game will automatically start in ${delay} seconds...`);
        await sleep(1000 * delay);
      }

      try {
        game.start(data);
        console.log('Game started!');
      } catch (err) {
        console.error('Cannot start the game: ');
        console.error(err.message);
        process.exit(1);
      }

      process.exit(0);
    } catch (err) {
      console.error('Cannot prepare the game: ');
      console.error(err.message);
      process.exit(1);
    }
  });

program.parse();
