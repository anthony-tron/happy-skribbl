const io = require('socket.io-client');

/**
 * Connects to the `origin` server and creates a game using `params`.
 * Returns the key of the game and an async function to start the game.
 *
 * @param {string} origin an URI of the server of the game
 * @param {object} params array passed to event `userData`
 * @returns {Promise<{ key: string, start: (customWords: string[]) => Promise<void> }>}
 */
const createGame = (origin, params) =>
  new Promise((resolve) => {
    const socket = io(origin);

    socket.on('result', (value) => {
      const host = io(value.host);

      host.on('connect', () => {
        host.emit('userData', params);
        host.emit('lobbySetCustomWordsExclusive', true);
      });

      host.on('lobbyConnected', (value) => {
        resolve({
          key: value.key,
          start: async (customWords) => {
            host.emit('lobbyGameStart', customWords);
            await host.close();
          },
        });
      });

      socket.close();
    });

    socket.emit('login', params);
  });

module.exports = { createGame };
