/**
 * Waits until `ms` milliseconds have elapsed.
 *
 * @param ms
 * @returns {Promise<() => void>}
 */
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

/**
 * Waits until event 'data' is emitted.
 *
 * @note when using with `process.stdin`, you will need to call `process.exit` because the process will not exit on its own
 *
 * @param {ReadableStream} stream
 * @returns {Promise<() => void>}
 */
const pause = (stream) =>
  new Promise((resolve) => stream.once('data', resolve));

module.exports = { sleep, pause };
